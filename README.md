## Ches(sic!) website

A [Ches(sic)!](https://gitlab.com/f1uctus/chessic) game framework website (client) implementation.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
