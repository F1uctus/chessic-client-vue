const toggleMenuOpen = `<path fill-rule="evenodd" clip-rule="evenodd" d="M19 2.14193H0V0.996094H19V2.14193ZM19 8.57552H0V7.42969H19V8.57552ZM0 15.0013H19V13.8555H0V15.0013Z" fill="white" />`
const toggleMenuClose = `<path fill-rule="evenodd" clip-rule="evenodd" d="M7.77698 8.7747L14.1421 15.1399L14.9524 14.3296L8.5872 7.96448L15.0711 1.48059L14.2609 0.670365L7.77698 7.15425L0.810227 0.1875L0 0.997727L6.96675 7.96448L0.118729 14.8125L0.928955 15.6227L7.77698 8.7747Z" fill="white" />`

export {
  toggleMenuOpen,
  toggleMenuClose
}