// @ts-ignore
import styled, {css, keyframes} from 'vue3-styled-components';

const rotateAnimation = keyframes`
  0% {
    transform: rotateX(-37.5deg) rotateY(45deg);
  }
  100% {
    transform: rotateX(-37.5deg) rotateY(405deg);
  }
`

export const Container = styled('div', {size: Number})`
  position: absolute;
  width: ${p => p.size + 1}px;
  height: ${p => p.size + 1}px;

  * {
    position: absolute;
    width: ${p => p.size + 1}px;
    height: ${p => p.size + 1}px;
  }
`;

export const Sides = styled('div', {size: Number})`
  animation: ${rotateAnimation} 3s linear infinite;
  transform-style: preserve-3d;
  transform: rotateX(-37.5deg) rotateY(45deg);

  * {
    box-sizing: border-box;
    background-color: rgb(119, 242, 160);
    border: ${p => p.size / 10}px solid white;
  }
`;

export const Side = styled('div', {rotate: String, size: Number})`
  transform-origin: 50% 50%;
  transform: ${p => p.rotate} translateZ(${p => p.size / 2}px);
`;