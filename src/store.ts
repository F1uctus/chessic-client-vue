import {createStore} from "vuex";
import axios from "axios";
import {apiUrl} from "./constants";

const store = createStore({
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
  },
  mutations: {
    auth_request(state) {
      state.status = 'loading'
    },
    auth_success(state, token) {
      state.status = 'success'
      state.token = token
    },
    auth_error(state) {
      state.status = 'error'
    },
    auth_logout(state) {
      state.status = ''
      state.token = ''
    },
  },
  actions: {
    login({commit}, user) {
      return new Promise((resolve, reject) => {
        commit('auth_request')
        axios.post(apiUrl + 'login', user)
          .then(res => {
            const token = res.data.token
            localStorage.setItem('token', token)
            axios.defaults.headers.common['token'] = token
            commit('auth_success', token)
            resolve(res)
          })
          .catch(err => {
            commit('auth_error')
            localStorage.removeItem('token')
            reject(err)
          })
      })
    },
    join({commit}, user) {
      return new Promise((resolve, reject) => {
        commit('auth_request')
        axios.post(apiUrl + 'join', user)
          .then(res => {
            const token = res.data.token
            localStorage.setItem('token', token)
            axios.defaults.headers.common['token'] = token
            commit('auth_success', res.data)
            resolve(res)
          })
          .catch(err => {
            commit('auth_error', err)
            localStorage.removeItem('token')
            reject(err)
          })
      })
    },
    logout({commit}) {
      return new Promise((resolve, reject) => {
        axios.post(apiUrl + 'logout')
          .then(res => {
            commit('auth_logout')
            localStorage.removeItem('token')
            delete axios.defaults.headers.common['token']
            resolve(res)
          })
      })
    },
  },
  getters: {
    isLoggedIn: state => !!state.token
  }
})

export default store;