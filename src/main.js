import {createApp} from 'vue'
import App from './App.vue'
import './index.scss'
import './styles/buttons/main.scss'
import './styles/radiogroup/index.scss'
import router from "./router";
import store from "./store";

const app = createApp(App)

// Directives
app.directive('click-outside', {
  beforeMount(el, binding) {
    el.event = function (event) {
      if (el !== event.target && !el.contains(event.target)) {
        binding.value(event, el);
      }
    }
    document.body.addEventListener('click', el.event)
  },
  unmounted(el) {
    document.body.removeEventListener('click', el.event)
  }
})

app.use(router)
app.use(store)

app.mount('#app')
