import {createRouter, createWebHistory} from "vue-router";
import store from "./store";

const routes = [
  {
    path: "/",
    component: () => import("./views/Home.vue"),
  }, {
    path: "/explore",
    meta: {
      requiresAuth: true
    },
    component: () => import("./views/Explore.vue"),
  }, {
    path: "/sandbox",
    component: () => import("./views/Sandbox.vue"),
  }, {
    path: "/join",
    component: () => import("./views/Join.vue"),
  }, {
    path: "/logout",
    meta: {
      requiresAuth: true
    },
    beforeEnter(to, from, next) {
      store.dispatch("logout")
      next()
    },
    component: () => import("./views/Join.vue"),
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/join')
  } else {
    next()
  }
})

export default router;