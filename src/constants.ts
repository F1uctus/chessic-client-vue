import {stripIndent} from "common-tags";

export const apiUrl = 'http://192.168.43.129:3001/api/'

export const settingDefaults = {
  maps() {
    return {
      name: "New map",
      dimensions: {x: 10, y: 10, z: 1},
      formula: ""
    }
  },
  coalitions() {
    return {
      name: "New coalition",
      color: "#ffffff",
      theme: "",
      startDirection: "N"
    }
  },
  creatures() {
    return {
      name: "New creature",
      movements: [{
        target: "Any",
        paths: ["N"]
      }]
    }
  },
  arrangement() {
    return {
      creature: "New Creature",
      coalition: "New coalition",
      x: 0,
      y: 0
    }
  }
}

export const exemplarySetting = {
  main: {
    name: "New game",
    visibility: "Public",
    link: "None"
  },
  maps: [settingDefaults.maps()],
  coalitions: [settingDefaults.coalitions()],
  creatures: [settingDefaults.creatures()],
  arrangement: [settingDefaults.arrangement()],
  assets: []
}

export const chessSetting = {
  main: {
    name: "Classic chess",
    visibility: "public",
    link: ""
  },
  maps: [
    {
      name: "Global map",
      dimensions: {
        x: 8,
        y: 8,
        z: 2
      },
      formula: stripIndent`
            f (x, y, z) {
              return ((x % 2 + y % 2) % 2 == 0) ? "#52ccb8" : "#238ca1";
            }`
    },
  ],
  coalitions: [
    {
      name: "white",
      color: "#dddddd",
      theme: "default",
      startDirection: "S"
    }, {
      name: "black",
      color: "#333333",
      theme: "default",
      startDirection: "N"
    }
  ],
  creatures: [
    {
      name: "Pawn",
      movements: [{
        target: "Any",
        paths: ["S"]
      }, {
        target: "Enemy",
        paths: ["SW", "SE"]
      }]
    }, {
      name: "Rook",
      movements: [{
        target: "Any",
        paths: ["N+", "E+", "S+", "W+"]
      }]
    }, {
      name: "Knight",
      movements: [{
        target: "Any",
        paths: [
          "N N W", "N N E", "N W W", "N E E",
          "S S W", "S S E", "S W W", "S E E"
        ]
      }]
    }, {
      name: "Bishop",
      movements: [{
        target: "Any",
        paths: ["NE+", "NW+", "SE+", "SW+"]
      }]
    }, {
      name: "Queen",
      movements: [{
        target: "Any",
        paths: [
          "N+", "NE+", "NW+", "S+", "SE+", "SW+", "E+", "W+"
        ]
      }]
    }, {
      name: "King",
      movements: [{
        target: "Any",
        paths: [
          "N", "NE", "NW", "S", "SE", "SW", "E", "W"
        ]
      }]
    }
  ],
  arrangement: [
    {creature: "pawn", coalition: "white", x: 1, y: 2},
    {creature: "pawn", coalition: "white", x: 2, y: 2},
    {creature: "pawn", coalition: "white", x: 3, y: 2},
    {creature: "pawn", coalition: "white", x: 4, y: 2},
    {creature: "pawn", coalition: "white", x: 5, y: 2},
    {creature: "pawn", coalition: "white", x: 6, y: 2},
    {creature: "pawn", coalition: "white", x: 7, y: 2},
    {creature: "pawn", coalition: "white", x: 8, y: 2},
    {creature: "pawn", coalition: "black", x: 1, y: 7},
    {creature: "pawn", coalition: "black", x: 2, y: 7},
    {creature: "pawn", coalition: "black", x: 3, y: 7},
    {creature: "pawn", coalition: "black", x: 4, y: 7},
    {creature: "pawn", coalition: "black", x: 5, y: 7},
    {creature: "pawn", coalition: "black", x: 6, y: 7},
    {creature: "pawn", coalition: "black", x: 7, y: 7},
    {creature: "pawn", coalition: "black", x: 8, y: 7},
    {creature: "rook", coalition: "white", x: 1, y: 1},
    {creature: "rook", coalition: "white", x: 8, y: 1},
    {creature: "rook", coalition: "black", x: 1, y: 8},
    {creature: "rook", coalition: "black", x: 8, y: 8},
    {creature: "knight", coalition: "white", x: 2, y: 1},
    {creature: "knight", coalition: "white", x: 7, y: 1},
    {creature: "knight", coalition: "black", x: 2, y: 8},
    {creature: "knight", coalition: "black", x: 7, y: 8},
    {creature: "bishop", coalition: "white", x: 3, y: 1},
    {creature: "bishop", coalition: "white", x: 6, y: 1},
    {creature: "bishop", coalition: "black", x: 3, y: 8},
    {creature: "bishop", coalition: "black", x: 6, y: 8},
    {creature: "queen", coalition: "white", x: 5, y: 1},
    {creature: "queen", coalition: "black", x: 5, y: 8},
    {creature: "king", coalition: "white", x: 4, y: 1},
    {creature: "king", coalition: "black", x: 4, y: 8},
  ],
  assets: []
}